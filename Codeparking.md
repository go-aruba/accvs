# Codeparking

This File is to contain helpful code snippest


## Printing all Sheets with Content

```
// Print Sheet Names
sheets := xlsx.GetSheetList()


	// Foreach Sheet get all the rows
	for i, sheet := range sheets {
		fmt.Printf("Sheet index number %d with name %s\n\n", i, sheet)
		rows, _ := xlsx.GetRows(sheet)
		for i, row := range rows {
			fmt.Printf("Row %d: ", i)
			for _, colCell := range row {
				fmt.Print(colCell, ",")
			}
			fmt.Println()
		}
	}
```

## Write to file by writing each row seperatly

```
rows, _ := xlsx.GetRows("variables_sample")

csvfile, err := os.Create("myexport_sample.csv")
if err != nil {
	log.Fatalf("failed creating file: %s", err)
}
defer csvfile.Close()

// Write to export file
csvwriter := csv.NewWriter(csvfile)
for _, row := range rows {
	csvwriter.Write(row)
}
csvwriter.Flush()
csvfile.Close()
```


## Write to file by writing all rows at once

```
csvwriter := csv.NewWriter(csvfile)
csvwriter.WriteAll(rows)
```

## Write CSV to Standard OUT

```
csvwriter = csv.NewWriter(os.Stdout)
csvwriter.WriteAll(rows)
```

