package csv

import (
	"accsv/internal/model"
	"accsv/internal/utils"
	"encoding/csv"
	"fmt"
	"io"
	"os"
)

type Writer struct {
	Delimiter rune
}

// Write writes a DataSet to a specified CSV file path.
func (w Writer) Write(dataSet model.DataSet, outputPath string) error {
	file, err := os.Create(outputPath)
	if err != nil {
		return fmt.Errorf("failed to create CSV file: %w", err)
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	writer.Comma = w.Delimiter
	defer writer.Flush()

	// Write headers
	if err := writer.Write(dataSet.Headers); err != nil {
		return fmt.Errorf("failed to write headers: %w", err)
	}

	// Write rows
	if err := writer.WriteAll(dataSet.Rows); err != nil {
		return fmt.Errorf("failed to write rows: %w", err)
	}

	return nil
}

// WriteFile is a helper function to write a DataSet to a CSV file using the specified delimiter.
func WriteFile(outputPath string, dataSet model.DataSet, delimiter rune) error {
	writer := Writer{Delimiter: delimiter}
	return writer.Write(dataSet, outputPath)
}

// ReadFile reads a CSV file and returns a slice of DataSets, interpreting the file as a single DataSet.
func ReadFile(inputPath string, delimiter rune) ([]model.DataSet, error) {
	file, err := os.Open(inputPath)
	if err != nil {
		return nil, fmt.Errorf("failed to open CSV file: %w", err)
	}
	defer file.Close()
	// Use splitFilename from utils package
	fileNameWithoutSuffix, _ := utils.SplitFilename(inputPath)

	return Read(file, fileNameWithoutSuffix, delimiter)
}

// Read reads a *os.file and returns a slice of DataSets, interpreting the filehandle as a single DataSet.
func Read(file io.Reader, sheetname string, delimiter rune) ([]model.DataSet, error) {
	csvReader := csv.NewReader(file)
	csvReader.Comma = delimiter
	rows, err := csvReader.ReadAll()
	if err != nil {
		return nil, fmt.Errorf("failed to read CSV file: %w", err)
	}

	ds := model.DataSet{
		SheetName: sheetname,
		Headers:   rows[0],
		Rows:      rows[1:],
	}

	return []model.DataSet{ds}, nil
}
