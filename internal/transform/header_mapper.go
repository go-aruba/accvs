package transform

import (
	"accsv/internal/utils"
	"fmt"
)

// createHeaderIndexMap creates a mapping of header names to their index in the input headers.
// This map is used to efficiently look up column indices for headers during transformation.
func createHeaderIndexMap(headers []string) (map[string]int, error) {
	if len(headers) == 0 {
		return nil, fmt.Errorf("no headers provided")
	}

	headerIndexMap := make(map[string]int)
	for i, header := range headers {
		headerIndexMap[header] = i
	}

	return headerIndexMap, nil
}

// identifyUnexpectedHeaders identifies headers in the input that are not in any of the required
// or optional fields of the TransformSet's HeaderMaps.
func identifyUnexpectedHeaders(inputHeaders []string, headerMaps []HeaderMap) []string {
	var unexpectedHeaders []string
	for _, header := range inputHeaders {
		found := false
		for _, headerMap := range headerMaps {
			// Due to the nature of the required and optional fields, we need to swap the keys and values
			// This is a workaround till we refactor the HeaderMap struct
			required := utils.SwapKeyValue(headerMap.RequiredFields)
			optional := utils.SwapKeyValue(headerMap.OptionalFields)
			if _, exists := required[header]; exists {
				found = true
				break
			}
			if _, exists := optional[header]; exists {
				found = true
				break
			}
		}
		if !found {
			unexpectedHeaders = append(unexpectedHeaders, header)
		}
	}

	return unexpectedHeaders
}
