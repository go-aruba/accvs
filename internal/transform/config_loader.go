package transform

// config_loader loads the TransformSet configuration from a file.

import (
	"encoding/json"
	"fmt"
	"os"
)

// readFile reads the file content from the given filePath and returns it as a byte slice.
func readFile(filePath string) ([]byte, error) {
	data, err := os.ReadFile(filePath)
	if err != nil {
		return nil, fmt.Errorf("failed to read config file: %w", err)
	}
	return data, nil
}

// readJSONFile reads a JSON file and parses it into a TransformSet.
func readJSONFile(filePath string) (TransformSet, error) {
	data, err := readFile(filePath)
	if err != nil {
		return TransformSet{}, err
	}
	return loadTransformSetJSON(data)
}

// loadTransformSetJSON parses the JSON data into a TransformSet.
func loadTransformSetJSON(data []byte) (TransformSet, error) {
	var transformSet TransformSet

	err := json.Unmarshal(data, &transformSet)
	if err != nil {
		return TransformSet{}, fmt.Errorf("failed to parse JSON config: %w", err)
	}

	return transformSet, nil
}
