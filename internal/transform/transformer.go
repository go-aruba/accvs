package transform

import (
	"accsv/internal/model"
	"log"
)

// transformDataSet applies the transformation rules from TransformSet to an input DataSet,
// producing an output DataSet with the desired headers and transformed rows.
func transformDataSet(input model.DataSet, transformConfig TransformSet) (model.DataSet, error) {
	output := model.DataSet{
		SheetName: "Transformed Data",
		Headers:   transformConfig.OutputHeaders,
		Rows:      [][]string{},
	}

	// Create a header index map for quick access to input headers
	headerIndexMap, err := createHeaderIndexMap(input.Headers)
	if err != nil {
		return model.DataSet{}, err
	}

	// Identify any unexpected headers
	unexpectedHeaders := identifyUnexpectedHeaders(input.Headers, transformConfig.HeaderMaps)

	// Log unexpected headers if any are found
	if len(unexpectedHeaders) > 0 {
		log.Printf("Warning: Unexpected headers found and ignored: %v\n", unexpectedHeaders)
	}

	// Process each row in the input DataSet according to the header mappings
	for _, record := range input.Rows {
		for _, headerMap := range transformConfig.HeaderMaps {
			row, allFieldsFilled := FillRequiredFields(record, transformConfig.OutputHeaders, headerMap, headerIndexMap)
			// Only add row if all required fields in the mapping are filled
			if allFieldsFilled {
				output.Rows = append(output.Rows, row)
			}
		}
	}

	return output, nil
}

// fillRequiredFields fills required fields in the row and checks if all required fields are present.
// Returns false if any required field is missing.
func fillRequiredFields(record []string, row []string, outputHeaders []string, requiredFields RequiredFields, indexMap map[string]int) bool {
	allFieldsFilled := true

	for i, header := range outputHeaders {
		if inputHeader, exists := requiredFields[header]; exists {
			if idx, found := indexMap[inputHeader]; found && idx < len(record) && record[idx] != "" {
				row[i] = record[idx]
			} else {
				row[i] = ""
				allFieldsFilled = false // Mark as false if any required field is missing
			}
		}
	}

	return allFieldsFilled
}

// fillOptionalFields fills optional fields in the row if they are present in the record.
func fillOptionalFields(record []string, row []string, outputHeaders []string, optionalFields OptionalFields, indexMap map[string]int) {
	for i, header := range outputHeaders {
		if inputHeader, exists := optionalFields[header]; exists {
			if idx, found := indexMap[inputHeader]; found && idx < len(record) && record[idx] != "" {
				if i < len(row) {
					row[i] = record[idx]
				}
			}
		}
	}
}
