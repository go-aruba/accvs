package transform

import (
	"accsv/internal/model"
)

// RequiredFields represents the mapping for required fields in the transformation.
// The key is the output header, and the value is the corresponding input header.
type RequiredFields map[string]string

// OptionalFields represents the mapping for optional fields in the transformation.
// The key is the output header, and the value is the corresponding input header.
type OptionalFields map[string]string

// HeaderMap defines a mapping structure for a set of required and optional fields.
// HeaderMap allows flexible mapping between input and output headers.
type HeaderMap struct {
	RequiredFields RequiredFields `json:"requiredFields"`
	OptionalFields OptionalFields `json:"optionalFields"`
}

// TransformSet represents the configuration for transforming a DataSet.
// It includes the headers for the output dataset and the header mappings.
type TransformSet struct {
	OutputHeaders []string    `json:"outputHeaders"` // Headers for the transformed output
	HeaderMaps    []HeaderMap `json:"headerMaps"`    // List of HeaderMaps to apply to input data
}

// TransformDataSet is the exported function that external packages call to transform a DataSet.
// It uses the internal transformDataSet function to perform the actual transformation logic.
func TransformDataSet(input model.DataSet, transformConfig TransformSet) (model.DataSet, error) {
	return transformDataSet(input, transformConfig)
}

// LoadTransformSetJSONFile is the exported function for loading the TransformSet configuration from a file.
// It serves as a wrapper for the internal loadTransformSet function.
func LoadTransformSetFile(filePath string) (TransformSet, error) {
	return readJSONFile(filePath)
}

// LoadTransformSet is the exported function for loading the TransformSet configuration from a file.
// It serves as a wrapper for the internal loadTransformSet function.
func LoadTransformSet(data []byte) (TransformSet, error) {
	return loadTransformSetJSON(data)

}

// FillRequiredFields is the exported function that fills both required and optional fields
// based on the header map configuration. It returns the filled row and a boolean indicating
// if all required fields are filled.
func FillRequiredFields(record []string, outputHeaders []string, headerMap HeaderMap, indexMap map[string]int) ([]string, bool) {
	row := make([]string, len(outputHeaders))
	// Fill required fields and check if all are filled
	allFieldsFilled := fillRequiredFields(record, row, outputHeaders, headerMap.RequiredFields, indexMap)

	// Fill optional fields
	fillOptionalFields(record, row, outputHeaders, headerMap.OptionalFields, indexMap)

	return row, allFieldsFilled
}
