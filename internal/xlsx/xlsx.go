package xlsx

import (
	"accsv/internal/model"
	"accsv/internal/utils"
	"fmt"

	"github.com/xuri/excelize/v2"
)

// ReadFile reads an XLSX file and returns an array of DataSets.
// Each DataSet includes headers, rows, and a sheet name derived from the input file.
func ReadFile(inputPath string, sheet string) ([]model.DataSet, error) {
	xlsx, err := excelize.OpenFile(inputPath)
	if err != nil {
		return nil, fmt.Errorf("failed to open XLSX file: %w", err)
	}

	// Use the first sheet if no specific sheet is provided.
	if sheet == "" {
		sheet = xlsx.GetSheetList()[0]
	}

	rows, err := xlsx.GetRows(sheet)
	if err != nil {
		return nil, fmt.Errorf("failed to read XLSX sheet '%s': %w", sheet, err)
	}

	// Get the sheet name based on the input file using the splitFilename function from utils.
	fileNameWithoutSuffix, _ := utils.SplitFilename(inputPath)

	// Create a DataSet that includes the sheet name, headers, and rows.
	ds := model.DataSet{
		SheetName: fileNameWithoutSuffix,
		Headers:   rows[0],
		Rows:      rows[1:],
	}

	return []model.DataSet{ds}, nil
}

// Writer writes a single DataSet to an XLSX file using the given sheet name.
type Writer struct {
	SheetName string
}

// Write writes the provided DataSet into an XLSX file.
func (w Writer) Write(dataSet model.DataSet, outputPath string) error {
	xlsx := excelize.NewFile()

	// Use the shared createAndFillSheet function to add data to the sheet.
	err := createAndFillSheet(xlsx, w.SheetName, dataSet, true)
	if err != nil {
		return err
	}

	// Save the XLSX file.
	err = xlsx.SaveAs(outputPath)
	if err != nil {
		return fmt.Errorf("failed to save XLSX file: %w", err)
	}

	return nil
}

// MultipleWriter writes multiple DataSets into an XLSX file, either consolidating them into one sheet or creating separate sheets for each dataset.
type MultipleWriter struct {
	All         bool
	Consolidate bool
}

// Write writes multiple DataSets into an XLSX file. If Consolidate is true, the data is combined into a single sheet.
func (w MultipleWriter) Write(dataSets []model.DataSet, outputPath string) error {
	xlsx := excelize.NewFile()
	if w.Consolidate {
		// Use both values returned by utils.SplitFilename
		fileNameWithoutSuffix, _ := utils.SplitFilename(outputPath)
		fmt.Println("Consolidating data into a single sheet...")
		combinedDataSet := model.DataSet{
			Headers:   dataSets[0].Headers,
			Rows:      dataSets[0].Rows,
			SheetName: fileNameWithoutSuffix, // Use the output file name as sheet name
		}
		for _, dataSet := range dataSets[1:] {
			if !compareHeaders(combinedDataSet.Headers, dataSet.Headers) {
				return fmt.Errorf("headers do not match across datasets, cannot consolidate")
			}
			combinedDataSet.Rows = append(combinedDataSet.Rows, dataSet.Rows...)
		}
		err := createAndFillSheet(xlsx, combinedDataSet.SheetName, combinedDataSet, true)
		if err != nil {
			return err
		}
	} else {
		for idx, dataSet := range dataSets {
			isFirstSheet := idx == 0
			err := createAndFillSheet(xlsx, dataSet.SheetName, dataSet, isFirstSheet)
			if err != nil {
				return err
			}
		}
	}

	err := xlsx.SaveAs(outputPath)
	if err != nil {
		return fmt.Errorf("failed to save XLSX file: %w", err)
	}

	return nil
}

// createAndFillSheet creates a new sheet or renames "Sheet1" in the XLSX file and inserts data into it.
func createAndFillSheet(xlsx *excelize.File, sheetName string, dataSet model.DataSet, isFirstSheet bool) error {
	if isFirstSheet {
		// Rename the first sheet (Sheet1) to the provided sheet name.
		xlsx.SetSheetName("Sheet1", sheetName)
	} else {
		// Create a new sheet for subsequent datasets.
		_, err := xlsx.NewSheet(sheetName)
		if err != nil {
			return fmt.Errorf("error creating sheet: %w", err)
		}
	}

	// Insert data into the sheet.
	for i, row := range append([][]string{dataSet.Headers}, dataSet.Rows...) {
		for j, cell := range row {
			cellName, err := excelize.CoordinatesToCellName(j+1, i+1)
			if err != nil {
				return fmt.Errorf("error converting coordinates: %w", err)
			}
			xlsx.SetCellValue(sheetName, cellName, cell)
		}
	}

	// Apply styling for the first row (header).
	style, err := xlsx.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#C0C0C0"}, // Gray background for header.
			Pattern: 1,
		},
		Font: &excelize.Font{
			Bold: true, // Bold font for headers.
		},
	})
	if err != nil {
		return fmt.Errorf("failed to create style: %w", err)
	}

	// Apply the style to the header row.
	lastCol, err := excelize.ColumnNumberToName(len(dataSet.Headers))
	if err != nil {
		return fmt.Errorf("failed to get last column: %w", err)
	}

	err = xlsx.SetCellStyle(sheetName, "A1", fmt.Sprintf("%s1", lastCol), style)
	if err != nil {
		return fmt.Errorf("failed to apply style: %w", err)
	}

	// Apply an autofilter to the header row.
	err = xlsx.AutoFilter(sheetName, fmt.Sprintf("A1:%s1", lastCol), nil)
	if err != nil {
		return fmt.Errorf("failed to apply autofilter: %w", err)
	}

	return nil
}

// compareHeaders compares two header slices to ensure they are identical.
func compareHeaders(h1, h2 []string) bool {
	if len(h1) != len(h2) {
		return false
	}
	for i := range h1 {
		if h1[i] != h2[i] {
			return false
		}
	}
	return true
}
