package utils

import (
	"path/filepath"
	"strings"
)

// SplitFilename takes an input file path and returns the file name without the suffix and the file extension (suffix).
func SplitFilename(inputFile string) (fileNameWithoutSuffix, suffix string) {
	suffix = strings.ToLower(filepath.Ext(inputFile))
	fileNameWithoutSuffix = strings.TrimSuffix(filepath.Base(inputFile), suffix)
	return fileNameWithoutSuffix, suffix
}

func SwapKeyValue(input map[string]string) map[string]string {
	swapped := make(map[string]string)

	for key, value := range input {
		swapped[value] = key
	}

	return swapped
}
