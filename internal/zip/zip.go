package zip

import (
	"accsv/internal/model"
	"accsv/internal/utils"
	"archive/zip"
	"encoding/csv"
	"fmt"
	"os"
	"strings"
)

// Writer is responsible for writing a dataset into a ZIP file.
type Writer struct {
	Delimiter rune
}

// Write writes a single dataset into a ZIP file containing a CSV file.
func (w Writer) Write(dataSet model.DataSet, outputPath string) error {
	zipFile, err := os.Create(outputPath)
	if err != nil {
		return fmt.Errorf("failed to create ZIP file: %w", err)
	}
	defer zipFile.Close()

	zipWriter := zip.NewWriter(zipFile)
	defer zipWriter.Close()

	// Create a CSV inside the ZIP
	csvWriter, err := zipWriter.Create(dataSet.SheetName + ".csv")
	if err != nil {
		return fmt.Errorf("failed to create CSV in ZIP: %w", err)
	}

	csvContent := strings.Join(dataSet.Headers, string(w.Delimiter)) + "\n"
	for _, row := range dataSet.Rows {
		csvContent += strings.Join(row, string(w.Delimiter)) + "\n"
	}

	_, err = csvWriter.Write([]byte(csvContent))
	if err != nil {
		return fmt.Errorf("failed to write CSV content to ZIP: %w", err)
	}

	return nil
}

// ReadFile reads all CSV files from a ZIP archive and returns the data as DataSets.
func ReadFile(zipFile, delimiter, sheet string, all bool) ([]model.DataSet, error) {
	r, err := zip.OpenReader(zipFile)
	if err != nil {
		return nil, fmt.Errorf("could not open zip file: %w", err)
	}
	defer r.Close()

	var dataSets []model.DataSet

	if !all {
		// If "all" flag is not set, only the file matching the ZIP filename will be loaded
		fileNameWithoutSuffix, _ := utils.SplitFilename(zipFile)
		for _, file := range r.File {
			if file.Name == fileNameWithoutSuffix+".csv" {
				rc, err := file.Open()
				if err != nil {
					return nil, fmt.Errorf("could not open file %s in zip: %w", file.Name, err)
				}
				defer rc.Close()

				csvReader := csv.NewReader(rc)
				csvReader.Comma = rune(delimiter[0])
				records, err := csvReader.ReadAll()
				if err != nil {
					return nil, fmt.Errorf("failed to read CSV file in zip: %w", err)
				}

				dataSet := model.DataSet{
					SheetName: fileNameWithoutSuffix,
					Headers:   records[0],
					Rows:      records[1:],
				}
				dataSets = append(dataSets, dataSet)
			}
		}
	} else {
		// Process all CSV files in the ZIP
		for _, file := range r.File {
			if strings.HasSuffix(file.Name, ".csv") {
				rc, err := file.Open()
				if err != nil {
					return nil, fmt.Errorf("could not open file %s in zip: %w", file.Name, err)
				}
				defer rc.Close()

				csvReader := csv.NewReader(rc)
				csvReader.Comma = rune(delimiter[0])
				records, err := csvReader.ReadAll()
				if err != nil {
					return nil, fmt.Errorf("failed to read CSV file in zip: %w", err)
				}

				// Use the file name without ".csv" as the sheet name.
				fileNameWithoutSuffix := strings.TrimSuffix(file.Name, ".csv")
				dataSet := model.DataSet{
					SheetName: fileNameWithoutSuffix,
					Headers:   records[0],
					Rows:      records[1:],
				}
				dataSets = append(dataSets, dataSet)
			}
		}
	}

	return dataSets, nil
}
