package model

// DataSet represents a set of data with headers and rows, including the associated sheet name
type DataSet struct {
	SheetName string     // The name of the sheet or dataset
	Headers   []string   // The headers for the dataset
	Rows      [][]string // The rows of data
}
