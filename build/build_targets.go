//go:build mage
// +build mage

package main

import (
	"fmt"
	"sync"
)

//////////////////////////////////////////////////////////////
// BUILD TARGETS
/////////////////////////////////////////////////////////////

// BuildAll builds all targets in parallel based on TargetConfig.
func BuildAll() error {
	config := TargetConfig{
		{Name: "accsv", BuildFunc: BuildAccsv},
		{Name: "danis2iserv", BuildFunc: BuildDanis2iserv},
	}

	var wg sync.WaitGroup
	errChan := make(chan error, len(config))

	for _, target := range config {
		wg.Add(1)
		go func(target Target) {
			defer wg.Done()
			fmt.Printf("Building TARGET %s...\n", target.Name)
			if err := target.BuildFunc(); err != nil {
				errChan <- fmt.Errorf("failed to build %s: %w", target.Name, err)
			}
		}(target)
	}

	wg.Wait()
	close(errChan)

	for err := range errChan {
		if err != nil {
			return err
		}
	}
	return nil
}

// BuildAccsv builds the accsv program and shows full output.
func BuildAccsv() error {
	return buildProgram("accsv")
}

// BuildDanis2iserv builds the danis2iserv program and shows full output.
func BuildDanis2iserv() error {
	return buildProgram("danis2iserv")
}
