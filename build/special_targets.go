//go:build mage
// +build mage

package main

import (
	"fmt"
	"os/exec"
	"path/filepath"
)

//////////////////////////////////////////////////////////////
// SPECIAL TARGETS
/////////////////////////////////////////////////////////////

// Testdata builds the accsv binary and creates test data for manual testing.
func Testdata() error {
	accsv := "accsv.exe"
	sample := filepath.Join("..", "testdata", "variables_sample.xlsx")
	cmd := exec.Command(accsv, "-i", sample)

	// Set the working directory to where you want the output files to be created
	cmd.Dir = filepath.Join("..", "bin")

	fmt.Printf("Executing %s -i %s for CSV file creation in the bin directory for easier manual testing.\n", accsv, sample)
	err := cmd.Run()
	if err != nil {
		return fmt.Errorf("failed to execute accsv: %w", err)
	}
	return nil
}

// BuildAndTestdata builds the accsv binary and creates test data for manual testing.
func BuildAndTestdata() error {
	err := BuildAccsv()
	if err != nil {
		return err
	}
	return Testdata()
}
