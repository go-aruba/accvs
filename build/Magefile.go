//go:build mage
// +build mage

// Package main - Mage Build Script
//
// This Mage build script is divided into multiple files to improve readability and maintainability.
// The targets are organized by task type and defined in separate files as follows:
//
// - build_targets.go:       Contains all build targets, such as `BuildAll`, `BuildAccsv`, and `BuildDanis2iserv`.
// - test_targets.go:        Includes test targets, like `TestAll`, `TestAccsv`, and `TestDanis2iserv`.
// - cleanup_targets.go:     Defines cleanup functions, such as `Clean`, to remove build artifacts.
// - special_targets.go:     Contains special tasks, such as `Testdata` and `BuildAndTestdata`.
//
// This file (`Makefile.go`) contains general definitions and internal helper functions, which
// are used by all other files to control the behavior and configuration of the build system.
//
// Usage:
// - All files must be in the same directory (no subdirectories).
// - Mage automatically recognizes all functions in the `main` package as targets.
//
// Example commands:
//   mage BuildAll       - Runs all build targets
//   mage TestAll        - Runs all test targets
//   mage Clean          - Cleans up build artifacts
//   mage Testdata       - Generates test data for manual verification

package main

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
)

//////////////////////////////////////////////////////////////
// DEFINITIONS
/////////////////////////////////////////////////////////////

var defaultVersion = "unknown"

// TargetConfig represents a list of build and test targets.
type TargetConfig []Target

// Target represents a single build or test target.
type Target struct {
	Name      string
	BuildFunc func() error
	TestFunc  func() error
}

//////////////////////////////////////////////////////////////
// INTERNAL FUNCTIONS
/////////////////////////////////////////////////////////////

// readVersion reads the version from the file located at ../VERSION relative to this file.
func readVersion() string {
	// Construct the path to the VERSION file relative to accsv/build/
	versionFilePath := filepath.Join("..", "VERSION")

	data, err := os.ReadFile(versionFilePath)
	if err != nil {
		fmt.Printf("Warning: could not read VERSION file, using default version: %s\n", defaultVersion)
		return defaultVersion
	}
	return strings.TrimSpace(string(data))
}

// runGoCommand is a helper to execute go commands (build or test) with specified ldflags and args.
func runGoCommand(program, action string, args ...string) error {
	version := readVersion()
	ldflags := fmt.Sprintf(`-X main.Version=%s`, version)
	baseArgs := []string{action, "-v", "-ldflags", ldflags}
	baseArgs = append(baseArgs, args...)

	cmd := exec.Command("go", baseArgs...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	fmt.Printf("Running %s for %s...\n", action, program)
	return cmd.Run()
}

// buildProgram compiles a given program and shows full output.
func buildProgram(program string) error {
	binPath := filepath.Join("..", "bin", program)
	if runtime.GOOS == "windows" {
		binPath += ".exe"
	}
	return runGoCommand(program, "build", "-o", binPath, fmt.Sprintf("../cmd/%s", program))
}

// testProgram runs tests for a given program using runGoCommand to set ldflags and log output.
func testProgram(program string) error {
	cmdDir := filepath.Join("..", "cmd", program)
	args := []string{"-count=1", "./..."} // -count=1 disables test caching

	// Use runGoCommand for the test command with the specified working directory
	err := os.Chdir(cmdDir) // Change working directory
	if err != nil {
		return fmt.Errorf("failed to change directory: %w", err)
	}
	defer os.Chdir("..") // Revert back after execution

	return runGoCommand(program, "test", args...)
}
