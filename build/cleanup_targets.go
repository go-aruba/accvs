//go:build mage
// +build mage

package main

import (
	"fmt"
	"os"
)

//////////////////////////////////////////////////////////////
// CLEANUP TARGETS
/////////////////////////////////////////////////////////////

// Clean removes built binaries and provides detailed feedback.
func Clean() error {
	err := os.RemoveAll("../bin")
	if err != nil && !os.IsNotExist(err) {
		return fmt.Errorf("failed to clean ../bin: %w", err)
	}
	fmt.Println("Cleaned up successfully.")
	return nil
}
