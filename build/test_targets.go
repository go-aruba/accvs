//go:build mage
// +build mage

package main

import "fmt"

//////////////////////////////////////////////////////////////
// TEST TARGETS
/////////////////////////////////////////////////////////////

// TestAll runs all test targets sequentially based on TargetConfig.
func TestAll() error {
	config := TargetConfig{
		//{Name: "accsv", TestFunc: TestAccsv},
		{Name: "danis2iserv", TestFunc: TestDanis2iserv},
	}

	for _, target := range config {
		fmt.Printf("Testing TARGET %s...\n", target.Name)
		if err := target.TestFunc(); err != nil {
			return fmt.Errorf("failed to test %s: %w", target.Name, err)
		}
	}
	return nil
}

/*
// TestAccsv runs tests for the accsv program.
func TestAccsv() error {
	return testProgram("accsv")
}
*/

// TestDanis2iserv runs tests for the danis2iserv program.
func TestDanis2iserv() error {
	return testProgram("danis2iserv")
}
