# accsv Documentation

---

## ⚠️ WARNING

Currently, the program does not check if the output file exists. As a result, the output file will be **overwritten** without any warnings.

---

## Excel to CSV and CSV to Excel Exporter

### Info

This program was created for Aruba Central files, but now it has a more general purpose.


### Excel to CSV

Aruba Central uses CSV or JSON files as key-value configurations for devices. When using Excel in a German language format, exported CSV files are not comma-separated (`,`). Instead, they are semicolon-separated (`;`), making them incompatible with Aruba Central uploads. 

Additionally, Excel doesn’t export text enclosed in double quotes (`"`), so replacing `;` with `,` in a text editor can lead to issues when values in a column contain commas.

This program addresses these issues by functioning as an **Excel to CSV** exporter with the required knowledge of the data structure.


### CSV to Excel

Excel in German format doesn’t import CSV files correctly. You are typically required to manually split the text into columns. This tool automates that process, ensuring that the CSV files are imported properly.



This program works as part of a toolchain together with [actemplater](https://gitlab.com/go-aruba/actemplater).


## Links

- [Excelize](https://xuri.me/excelize/en/)


## TODO

- **Fix zip export:** Currently, the name `output.csv` is used, and only one sheet is exported.
  - The `-a` option should export each sheet to a file named after the sheet (for multiple files, a zip file is required).
  - The `-a -c` option should export all sheets consolidated into one CSV file, and `-z` should be used for zipped output.
