package main

import (
	"accsv/internal/csv"
	"accsv/internal/transform"
	"accsv/internal/utils"
	"bytes"
	"embed"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
)

//go:embed config/default-config.json
var embeddedFiles embed.FS

// Version will be set during build time using -ldflags.
var Version = "dev" // Default value if not set by build

// Config struct to hold flag values and configuration.
type Config struct {
	InputFile   string
	OutputFile  string
	Delimiter   string
	ShowVersion bool
	PrintConfig bool
	ConfigFile  string
}

// parseFlags parses the command-line flags into the Config struct.
func parseFlags() *Config {
	cfg := &Config{}
	flag.StringVar(&cfg.InputFile, "i", "IServ_Elternexport.csv", "CSV input filename for transformation")
	flag.StringVar(&cfg.OutputFile, "o", "IServ_Elternimport.csv", "CSV output filename for transformed data")
	flag.StringVar(&cfg.Delimiter, "d", ";", "CSV delimiter (default is ';')")
	flag.BoolVar(&cfg.ShowVersion, "v", false, "Print program version")
	flag.BoolVar(&cfg.PrintConfig, "printconfig", false, "Print embedded config file for TransformSet")
	flag.StringVar(&cfg.ConfigFile, "c", "", "Path to configuration file")
	flag.Parse()
	return cfg
}

func main() {
	cfg := parseFlags()

	if cfg.ShowVersion {
		printVersion()
		return
	}

	if cfg.PrintConfig {
		printConfig()
		return
	}

	file, err := os.Open(cfg.InputFile)
	if err != nil {
		handleError(fmt.Errorf("failed to open CSV file: %w", err))
	}
	defer file.Close()

	// Get *os.File that guarantees UTF-8 without BOM
	// Danis is exporting UTF8-BOM encoded files, so we need to remove the BOM
	fileNoBOM, err := utf8NoBOMFile(file)
	if err != nil {
		handleError(fmt.Errorf("Error processing file for UTF-8 without BOM: %w", err))
		return
	}
	// Use splitFilename from utils package
	fileNameWithoutSuffix, _ := utils.SplitFilename(cfg.InputFile)

	// Load input dataset
	inputDataSet, err := csv.Read(fileNoBOM, fileNameWithoutSuffix, rune(cfg.Delimiter[0]))
	handleError(err)

	// Load transformation configuration
	var transformSet transform.TransformSet
	if cfg.ConfigFile == "" {
		// Load embedded default configuration if no config file is specified
		data, err := embeddedFiles.ReadFile("config/default-config.json")
		handleError(err)
		transformSet, err = transform.LoadTransformSet(data)
		handleError(err)
	} else {
		// Load configuration from specified file path
		data, err := os.ReadFile(cfg.ConfigFile)
		handleError(err)
		transformSet, err = transform.LoadTransformSet(data)
		handleError(err)
	}

	// Apply the transformation
	transformedDataSet, err := transform.TransformDataSet(inputDataSet[0], transformSet)
	handleError(err)

	// Write the transformed dataset to output file
	err = csv.WriteFile(cfg.OutputFile, transformedDataSet, rune(cfg.Delimiter[0]))
	handleError(err)
}

// handleError logs the error and exits the program if err is not nil.
func handleError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

// utf8NoBOMFile returns *os.File without the UTF-8 BOM if present.
// Ensures the returned *os.File can be read as UTF-8 without BOM.
func utf8NoBOMFile(file *os.File) (*os.File, error) {
	// Define the UTF-8 BOM byte sequence
	UTF8BOM := []byte{0xEF, 0xBB, 0xBF}

	// Read the first 3 bytes to check for BOM
	buf := make([]byte, len(UTF8BOM))
	_, err := file.Read(buf)
	if err != nil && err != io.EOF {
		return nil, fmt.Errorf("error reading file: %w", err)
	}

	// Check if BOM is present
	if bytes.Equal(buf, UTF8BOM) {
		// BOM found, so the file pointer is already positioned after the BOM
		return file, nil
	}

	// No BOM found; reset file pointer to the beginning
	_, err = file.Seek(0, io.SeekStart)
	if err != nil {
		return nil, fmt.Errorf("error resetting file pointer: %w", err)
	}

	// Return *os.File positioned at the beginning
	return file, nil
}

// printVersion prints the current version of the program.
func printVersion() {
	fmt.Println("Version:", Version)
	os.Exit(0)
}

// printConfig outputs the contents of the embedded configuration file.
func printConfig() {
	data, err := embeddedFiles.ReadFile("config/default-config.json")
	if err != nil {
		log.Fatalf("Error reading embedded config: %v", err)
	}
	fmt.Println(string(data))
}
