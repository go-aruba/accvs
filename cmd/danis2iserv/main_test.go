package main

import (
	"bytes"
	"encoding/json"
	"os"
	"testing"

	"accsv/internal/model"
	"accsv/internal/transform"
)

// TestCase struct for organizing test inputs and expected outcomes.
type TestCase struct {
	Name            string
	Record          []string
	ExpectedRow     []string
	AllFieldsFilled bool
	OnlyOptional    bool
}

// TestFillRequiredFields tests FillRequiredFields using a table-driven approach.
func TestFillRequiredFields(t *testing.T) {
	indexMap := map[string]int{
		"firstname1": 0,
		"lastname1":  1,
		"street1":    2,
		"zipcode1":   3,
		"firstname2": 4,
		"lastname2":  5,
	}
	requiredFields := transform.RequiredFields{
		"firstname": "firstname1",
		"lastname":  "lastname1",
	}
	optionalFields := transform.OptionalFields{
		"zipcode": "zipcode1",
	}

	testCases := []TestCase{
		{
			Name:            "All required fields present",
			Record:          []string{"John", "Doe", "Some Street", "12345", "Jane", "Doe"},
			ExpectedRow:     []string{"John", "Doe"},
			AllFieldsFilled: true,
			OnlyOptional:    false,
		},
		{
			Name:            "Missing required field (firstname1)",
			Record:          []string{"", "Doe", "Some Street", "12345", "Jane", "Doe"},
			ExpectedRow:     []string{"", "Doe"},
			AllFieldsFilled: false,
			OnlyOptional:    false,
		},
		{
			Name:            "Only optional fields present",
			Record:          []string{"", "", "Some Street", "12345", "Jane", "Doe"},
			ExpectedRow:     []string{"", ""},
			AllFieldsFilled: false,
			OnlyOptional:    true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.Name, func(t *testing.T) {
			row, allFilled := transform.FillRequiredFields(tc.Record, []string{"firstname", "lastname"}, transform.HeaderMap{RequiredFields: requiredFields, OptionalFields: optionalFields}, indexMap)

			if allFilled != tc.AllFieldsFilled {
				t.Errorf("%s: expected allFieldsFilled to be %v, got %v", tc.Name, tc.AllFieldsFilled, allFilled)
			}

			for i, val := range row {
				if val != tc.ExpectedRow[i] {
					t.Errorf("%s: expected row[%d] to be %s, got %s", tc.Name, i, tc.ExpectedRow[i], val)
				}
			}
		})
	}
}

// TestDataSetTransformation checks if rows with missing required fields are excluded from the output.
func TestDataSetTransformation(t *testing.T) {
	transformSet := transform.TransformSet{
		OutputHeaders: []string{"firstname", "lastname", "street", "zipcode"},
		HeaderMaps: []transform.HeaderMap{
			{
				RequiredFields: transform.RequiredFields{
					"firstname": "firstname1",
					"lastname":  "lastname1",
				},
				OptionalFields: transform.OptionalFields{
					"zipcode": "zipcode1",
					"street":  "street1",
				},
			},
		},
	}

	inputDataSet := model.DataSet{
		Headers: []string{"firstname1", "lastname1", "street1", "zipcode1", "firstname2", "lastname2"},
		Rows: [][]string{
			{"John", "Doe", "Street 1", "12345", "Jane", "Doe"},
			{"", "Doe", "Street 2", "12345", "Jane", "Doe"},
			{"", "", "Street 3", "67890", "Jane", "Doe"},
		},
	}

	output, err := transform.TransformDataSet(inputDataSet, transformSet)
	if err != nil {
		t.Fatalf("Error transforming dataset: %v", err)
	}

	expectedRows := [][]string{
		{"John", "Doe", "Street 1", "12345"},
	}

	if len(output.Rows) != len(expectedRows) {
		t.Fatalf("Expected %d rows in output, but got %d", len(expectedRows), len(output.Rows))
	}

	for i, row := range output.Rows {
		for j, cell := range row {
			if cell != expectedRows[i][j] {
				t.Errorf("Mismatch at row %d, column %d: expected %s, got %s", i, j, expectedRows[i][j], cell)
			}
		}
	}
}

// TestPrintJSONConfig checks if the embedded JSON config is correctly printed.
func TestPrintJSONConfig(t *testing.T) {
	r, w, err := os.Pipe()
	if err != nil {
		t.Fatalf("Failed to create pipe: %v", err)
	}

	stdout := os.Stdout
	os.Stdout = w
	defer func() { os.Stdout = stdout }()

	printConfig() // assuming this function now outputs JSON

	w.Close()
	var buf bytes.Buffer
	_, err = buf.ReadFrom(r)
	if err != nil {
		t.Fatalf("Failed to read from pipe: %v", err)
	}

	if buf.Len() == 0 {
		t.Errorf("Expected JSON output, but got empty output")
	}

	// Attempt to unmarshal the JSON to confirm its validity
	var jsonData interface{}
	if err := json.Unmarshal(buf.Bytes(), &jsonData); err != nil {
		t.Errorf("Output is not valid JSON: %v", err)
	}
}
