package main

import (
	"accsv/internal/csv"
	"accsv/internal/model"
	"accsv/internal/utils"
	"accsv/internal/xlsx"
	"accsv/internal/zip"
	"flag"
	"fmt"
	"log"
	"os"
)

// Version will be set during build time using -ldflags.
var Version = "dev" // Default value if not set by build

// Config struct to hold flag values and configuration.
type Config struct {
	InputFile   string
	Delimiter   string
	Sheet       string
	Consolidate bool
	OutputAsZip bool
	All         bool
	Version     bool
}

// parseFlags parses the command-line flags into the Config struct.
func parseFlags() *Config {
	cfg := &Config{}
	flag.StringVar(&cfg.InputFile, "i", "", "Input Filename")
	flag.StringVar(&cfg.Delimiter, "d", ",", "CSV delimiter (default is ',')")
	flag.StringVar(&cfg.Sheet, "s", "", "Sheet name (default is the filename without suffix)")
	flag.BoolVar(&cfg.Consolidate, "c", false, "Consolidate all CSVs in the ZIP into one sheet")
	flag.BoolVar(&cfg.OutputAsZip, "z", false, "Export only the specified sheet as ZIP containing CSV")
	flag.BoolVar(&cfg.All, "a", false, "Export all Excel sheets as CSV or import all CSVs from ZIP")
	flag.BoolVar(&cfg.Version, "v", false, "Prints out Version")
	flag.Parse()
	return cfg
}

func main() {
	// Parse flags into the Config struct.
	cfg := parseFlags()

	if cfg.Version {
		printVersion()
		return
	}

	if cfg.InputFile == "" {
		log.Fatal("Input file path must be provided with the -i flag")
	}

	// Process the input file and retrieve datasets.
	dataSets, err := processInputFile(cfg)
	handleError(err)

	// Write the dataset to the output file based on the configuration.
	err = writeDatasetToFile(cfg, dataSets)
	handleError(err)
}

// processInputFile processes the input file (CSV, XLSX, ZIP) and returns datasets.
func processInputFile(cfg *Config) ([]model.DataSet, error) {
	var (
		dataSets []model.DataSet
		err      error
	)

	fileNameWithoutSuffix, suffix := utils.SplitFilename(cfg.InputFile)

	if cfg.Sheet == "" {
		cfg.Sheet = fileNameWithoutSuffix
	}

	switch suffix {
	case ".csv":
		dataSets, err = csv.ReadFile(cfg.InputFile, rune(cfg.Delimiter[0]))
	case ".xlsx":
		dataSets, err = xlsx.ReadFile(cfg.InputFile, cfg.Sheet)
	case ".zip":
		dataSets, err = zip.ReadFile(cfg.InputFile, cfg.Delimiter, cfg.Sheet, cfg.All)
	default:
		log.Fatal("Unsupported file type.")
	}

	if len(dataSets) == 0 {
		log.Fatal("No data found to write")
	}

	return dataSets, err
}

// writeDatasetToFile writes datasets to a file based on the configuration.
func writeDatasetToFile(cfg *Config, dataSets []model.DataSet) error {
	outputFile, suffix := utils.SplitFilename(cfg.InputFile)

	if cfg.OutputAsZip {
		return writeZipFile(cfg, dataSets, outputFile)
	}

	if suffix == ".csv" || suffix == ".zip" {
		return writeXLSXFile(cfg, dataSets, outputFile)
	}

	return writeCSVFile(cfg, dataSets, outputFile)
}

// writeZipFile writes datasets to a ZIP file.
func writeZipFile(cfg *Config, dataSets []model.DataSet, outputFile string) error {
	writer := zip.Writer{Delimiter: rune(cfg.Delimiter[0])}
	outputFile += ".zip"
	if cfg.Consolidate {
		dataSets[0].SheetName = outputFile // Consolidated output with zip filename as sheet name.
	}
	return writer.Write(dataSets[0], outputFile)
}

// writeXLSXFile writes datasets to an XLSX file, handling multiple sheets if necessary.
func writeXLSXFile(cfg *Config, dataSets []model.DataSet, outputFile string) error {
	if len(dataSets) > 1 {
		writer := xlsx.MultipleWriter{All: cfg.All, Consolidate: cfg.Consolidate}
		return writer.Write(dataSets, outputFile+".xlsx")
	}
	writer := xlsx.Writer{SheetName: dataSets[0].SheetName}
	return writer.Write(dataSets[0], outputFile+".xlsx")
}

// writeCSVFile writes a single dataset to a CSV file.
func writeCSVFile(cfg *Config, dataSets []model.DataSet, outputFile string) error {
	writer := csv.Writer{Delimiter: rune(cfg.Delimiter[0])}
	outputFile += ".csv"
	return writer.Write(dataSets[0], outputFile)
}

// handleError checks for an error and logs it, terminating the program if necessary.
func handleError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

// printVersion prints the current version of the program.
func printVersion() {
	fmt.Println("Version: ", Version)
	os.Exit(0)
}
